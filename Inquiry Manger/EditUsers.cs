﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.LookAndFeel;
using DevExpress.XtraLayout;

namespace Inquiry_Manger
{
    public partial class EditUsers : UserControl
    {
        public EditUsers()
        {
            LayoutControl lc = new LayoutControl();
            lc.Dock = DockStyle.Fill;
            tePassword.Properties.UseSystemPasswordChar = true;
            chk_EditAddUsers.Text = "Can Add / Edit Other users";
            lc.AddItem("User", lkp_Users).TextVisible = true;
            lc.AddItem("UserName", teLogin).TextVisible = true;
            lc.AddItem("Password", tePassword).TextVisible = true;
            lc.AddItem(" ", chk_EditAddUsers).TextVisible = true;
            this.Controls.Add(lc);
            this.Size = new System.Drawing.Size(300, 125);
            lkp_Users.EditValueChanged += Lkp_Users_EditValueChanged;
            DataTable DT = Data.MainDataSet.Tables[6].Copy();

            lkp_Users.Enabled = Main.Isadmin;
            chk_EditAddUsers.Enabled = Main.Isadmin;
            DT.Columns.Remove("Code");
            
            lkp_Users.Properties.DataSource = DT;
            lkp_Users.Properties.DisplayMember = "Name";
            lkp_Users.Properties.ValueMember = "Name";
            DataRow[] rows = Data.MainDataSet.Tables[6].Select(" Name = '" + Main.CurrentUser + "'");
            if (rows.Count<DataRow>() == 1)
            {
                lkp_Users.EditValue = rows[0][0];
            }
       
           
        }

        private void Lkp_Users_EditValueChanged(object sender, EventArgs e)
        {
            DataRow[] rows = Data.MainDataSet.Tables[6].Select(" Name = '" + lkp_Users.EditValue + "'");
            if (rows.Count<DataRow>() == 1)
            {
                teLogin.Text = rows[0][0].ToString();
                tePassword.Text = rows[0][1].ToString();
                chk_EditAddUsers.EditValue = Convert.ToBoolean ( rows[0][3]);
            }
           
        }

        public TextEdit teLogin = new TextEdit();
        public LookUpEdit lkp_Users = new LookUpEdit();
        public TextEdit tePassword = new TextEdit();
        public CheckEdit chk_EditAddUsers = new CheckEdit();
        public static void ShowLogin()
        {
            EditUsers  myControl = new EditUsers();
            UserLookAndFeel lkf = new UserLookAndFeel(myControl);
            lkf.SkinName = "The Bezier";
            ReEnter:
            switch (DevExpress.XtraEditors.XtraDialog.Show(lkf, myControl, "Edit User ", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button1, 0))
            {
                case DialogResult.OK:

                    if (myControl.teLogin.Text.Trim() != "" && myControl.tePassword.Text.Trim() != "")
                    {
                        DataRow[] rows = Data.MainDataSet.Tables[6].Select(" Name = '" + myControl.lkp_Users.EditValue + "'");
                        if (rows.Count<DataRow>() == 1)
                        {
                           rows[0][0] = myControl.teLogin.Text;
                           rows[0][1] = myControl.tePassword.Text;
                           rows[0][2] = Main.CurrentUser;
                           rows[0][3] = myControl.chk_EditAddUsers.Checked;
                           Data.SaveMainDataFile();
                            Data.ReadMainDataFile();
                        }

                        
                      
                     
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(text: "Username and Password fields can't be empty ", caption: "Error", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Error, defaultButton: 0);
                        goto ReEnter;
                    }
                    ; break;

            }


        }
        private void EditUsers_Load(object sender, System.EventArgs e)
        {
           


        }
    }
}
