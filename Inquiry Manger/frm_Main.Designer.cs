﻿namespace Inquiry_Manger
{
    partial class frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Main));
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.btn_Print = new DevExpress.XtraBars.BarButtonItem();
            this.btn_Officers = new DevExpress.XtraBars.BarButtonItem();
            this.btn_Branches = new DevExpress.XtraBars.BarButtonItem();
            this.btn_Customers = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.btn_AddUsers = new DevExpress.XtraBars.BarButtonItem();
            this.btn_ChangeUser = new DevExpress.XtraBars.BarButtonItem();
            this.cb_Status = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btn_Save = new DevExpress.XtraBars.BarButtonItem();
            this.btn_New = new DevExpress.XtraBars.BarButtonItem();
            this.btn_Delete = new DevExpress.XtraBars.BarButtonItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.galleryDropDown1 = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.lkp_Branch = new DevExpress.XtraEditors.LookUpEdit();
            this.lkp_Officar = new DevExpress.XtraEditors.LookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.txt_Code = new DevExpress.XtraEditors.TextEdit();
            this.memoEdit_Notes = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit_Desc = new DevExpress.XtraEditors.MemoEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.lkp_Customer = new DevExpress.XtraEditors.LookUpEdit();
            this.lookAndFeelSettingsHelper1 = new WindowsApplication1.LookAndFeelSettingsHelper();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Status.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Branch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Officar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Notes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Desc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Customer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(12, 536);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(32, 16);
            this.labelControl7.TabIndex = 21;
            this.labelControl7.Text = "Notes";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(12, 366);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(63, 16);
            this.labelControl6.TabIndex = 27;
            this.labelControl6.Text = "Description";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(12, 335);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(26, 16);
            this.labelControl8.TabIndex = 26;
            this.labelControl8.Text = "Date";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(12, 303);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 16);
            this.labelControl5.TabIndex = 25;
            this.labelControl5.Text = "Status";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 270);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(39, 16);
            this.labelControl4.TabIndex = 24;
            this.labelControl4.Text = "Branch";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 238);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(38, 16);
            this.labelControl3.TabIndex = 23;
            this.labelControl3.Text = "Officar";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 206);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(55, 16);
            this.labelControl2.TabIndex = 22;
            this.labelControl2.Text = "Customer";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 174);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(29, 16);
            this.labelControl1.TabIndex = 28;
            this.labelControl1.Text = "Code";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btn_Print);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Reporting";
            // 
            // btn_Print
            // 
            this.btn_Print.Caption = "Print";
            this.btn_Print.Id = 8;
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn_Print_ItemClick);
            // 
            // btn_Officers
            // 
            this.btn_Officers.Caption = "Officars";
            this.btn_Officers.Id = 6;
            this.btn_Officers.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_Officers.ImageOptions.Image")));
            this.btn_Officers.Name = "btn_Officers";
            this.btn_Officers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn_Officars_ItemClick);
            // 
            // btn_Branches
            // 
            this.btn_Branches.Caption = "Branches";
            this.btn_Branches.Id = 5;
            this.btn_Branches.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_Branches.ImageOptions.Image")));
            this.btn_Branches.Name = "btn_Branches";
            this.btn_Branches.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn_Branches_ItemClick);
            // 
            // btn_Customers
            // 
            this.btn_Customers.Caption = "Customers";
            this.btn_Customers.Id = 4;
            this.btn_Customers.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_Customers.ImageOptions.Image")));
            this.btn_Customers.Name = "btn_Customers";
            this.btn_Customers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn_Customers_ItemClick);
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btn_Customers);
            this.ribbonPageGroup2.ItemLinks.Add(this.btn_Branches);
            this.ribbonPageGroup2.ItemLinks.Add(this.btn_Officers);
            this.ribbonPageGroup2.ItemLinks.Add(this.btn_AddUsers);
            this.ribbonPageGroup2.ItemLinks.Add(this.btn_ChangeUser);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Data";
            // 
            // btn_AddUsers
            // 
            this.btn_AddUsers.Caption = "Add User";
            this.btn_AddUsers.Id = 10;
            this.btn_AddUsers.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddUsers.ImageOptions.Image")));
            this.btn_AddUsers.Name = "btn_AddUsers";
            this.btn_AddUsers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn_AddUsers_ItemClick);
            // 
            // btn_ChangeUser
            // 
            this.btn_ChangeUser.Caption = "Update User";
            this.btn_ChangeUser.Id = 11;
            this.btn_ChangeUser.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_ChangeUser.ImageOptions.Image")));
            this.btn_ChangeUser.Name = "btn_ChangeUser";
            this.btn_ChangeUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn_ChangeUser_ItemClick);
            // 
            // cb_Status
            // 
            this.cb_Status.EditValue = "Open";
            this.cb_Status.Location = new System.Drawing.Point(86, 300);
            this.cb_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cb_Status.MenuManager = this.ribbonControl1;
            this.cb_Status.Name = "cb_Status";
            this.cb_Status.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Status.Properties.Items.AddRange(new object[] {
            "Open",
            "Closed"});
            this.cb_Status.Size = new System.Drawing.Size(332, 22);
            this.cb_Status.TabIndex = 29;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.btn_Save,
            this.btn_New,
            this.btn_Delete,
            this.btn_Customers,
            this.btn_Branches,
            this.btn_Officers,
            this.btn_Print,
            this.skinRibbonGalleryBarItem1,
            this.btn_AddUsers,
            this.btn_ChangeUser});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonControl1.MaxItemId = 13;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowCategoryInCaption = false;
            this.ribbonControl1.ShowDisplayOptionsMenuButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersInFormCaption = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl1.ShowQatLocationSelector = false;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(945, 150);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            // 
            // btn_Save
            // 
            this.btn_Save.Caption = "Save";
            this.btn_Save.Id = 1;
            this.btn_Save.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.ImageOptions.Image")));
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btn_Save.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn_Save_ItemClick);
            // 
            // btn_New
            // 
            this.btn_New.Caption = "New";
            this.btn_New.Id = 2;
            this.btn_New.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.ImageOptions.Image")));
            this.btn_New.Name = "btn_New";
            this.btn_New.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btn_New.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn_New_ItemClick);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Caption = "Delete";
            this.btn_Delete.Id = 3;
            this.btn_Delete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_Delete.ImageOptions.Image")));
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btn_Delete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn_Delete_ItemClick);
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            // 
            // 
            // 
            this.skinRibbonGalleryBarItem1.Gallery.ShowItemText = true;
            this.skinRibbonGalleryBarItem1.GalleryDropDown = this.galleryDropDown1;
            this.skinRibbonGalleryBarItem1.Id = 9;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.GalleryItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.skinRibbonGalleryBarItem1_GalleryItemClick);
            this.skinRibbonGalleryBarItem1.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.skinRibbonGalleryBarItem1_ItemPress);
            // 
            // galleryDropDown1
            // 
            this.galleryDropDown1.Name = "galleryDropDown1";
            this.galleryDropDown1.Ribbon = this.ribbonControl1;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup4});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btn_Save);
            this.ribbonPageGroup1.ItemLinks.Add(this.btn_New);
            this.ribbonPageGroup1.ItemLinks.Add(this.btn_Delete);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "File";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Skin";
            // 
            // lkp_Branch
            // 
            this.lkp_Branch.Location = new System.Drawing.Point(86, 267);
            this.lkp_Branch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lkp_Branch.Name = "lkp_Branch";
            this.lkp_Branch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp_Branch.Properties.NullText = "";
            this.lkp_Branch.Size = new System.Drawing.Size(332, 22);
            this.lkp_Branch.TabIndex = 20;
            // 
            // lkp_Officar
            // 
            this.lkp_Officar.Location = new System.Drawing.Point(86, 235);
            this.lkp_Officar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lkp_Officar.Name = "lkp_Officar";
            this.lkp_Officar.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp_Officar.Properties.NullText = "";
            this.lkp_Officar.Size = new System.Drawing.Size(332, 22);
            this.lkp_Officar.TabIndex = 19;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsDetail.EnableMasterViewMode = false;
            this.gridView1.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.PrintInitialize += new DevExpress.XtraGrid.Views.Base.PrintInitializeEventHandler(this.gridView1_PrintInitialize);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Location = new System.Drawing.Point(426, 169);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(507, 558);
            this.gridControl1.TabIndex = 18;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.Click += new System.EventHandler(this.gridControl1_Click);
            // 
            // txt_Code
            // 
            this.txt_Code.Location = new System.Drawing.Point(86, 171);
            this.txt_Code.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_Code.Name = "txt_Code";
            this.txt_Code.Size = new System.Drawing.Size(332, 22);
            this.txt_Code.TabIndex = 17;
            // 
            // memoEdit_Notes
            // 
            this.memoEdit_Notes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.memoEdit_Notes.Location = new System.Drawing.Point(86, 534);
            this.memoEdit_Notes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.memoEdit_Notes.Name = "memoEdit_Notes";
            this.memoEdit_Notes.Size = new System.Drawing.Size(332, 193);
            this.memoEdit_Notes.TabIndex = 16;
            this.memoEdit_Notes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.memoEdit_Desc_KeyDown);
            // 
            // memoEdit_Desc
            // 
            this.memoEdit_Desc.Location = new System.Drawing.Point(86, 364);
            this.memoEdit_Desc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.memoEdit_Desc.Name = "memoEdit_Desc";
            this.memoEdit_Desc.Size = new System.Drawing.Size(332, 162);
            this.memoEdit_Desc.TabIndex = 15;
            this.memoEdit_Desc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.memoEdit_Desc_KeyDown);
            this.memoEdit_Desc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.memoEdit_Desc_KeyPress);
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(86, 332);
            this.dateEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(332, 22);
            this.dateEdit1.TabIndex = 14;
            // 
            // lkp_Customer
            // 
            this.lkp_Customer.Location = new System.Drawing.Point(86, 203);
            this.lkp_Customer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lkp_Customer.Name = "lkp_Customer";
            this.lkp_Customer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp_Customer.Properties.NullText = "";
            this.lkp_Customer.Size = new System.Drawing.Size(332, 22);
            this.lkp_Customer.TabIndex = 13;
            // 
            // frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 740);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.cb_Status);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.lkp_Branch);
            this.Controls.Add(this.lkp_Officar);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.txt_Code);
            this.Controls.Add(this.memoEdit_Notes);
            this.Controls.Add(this.memoEdit_Desc);
            this.Controls.Add(this.dateEdit1);
            this.Controls.Add(this.lkp_Customer);
            this.LookAndFeel.SkinName = "The Bezier";
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inquiry Manger";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Main_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.StyleChanged += new System.EventHandler(this.Form1_StyleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.cb_Status.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Branch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Officar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Notes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Desc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Customer.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem btn_Officers;
        private DevExpress.XtraBars.BarButtonItem btn_Branches;
        private DevExpress.XtraBars.BarButtonItem btn_Customers;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Status;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem btn_Save;
        private DevExpress.XtraBars.BarButtonItem btn_New;
        private DevExpress.XtraBars.BarButtonItem btn_Delete;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraEditors.LookUpEdit lkp_Branch;
        private DevExpress.XtraEditors.LookUpEdit lkp_Officar;
        private DevExpress.XtraEditors.LookUpEdit lkp_Customer;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.MemoEdit memoEdit_Desc;
        private DevExpress.XtraEditors.MemoEdit memoEdit_Notes;
        private DevExpress.XtraEditors.TextEdit txt_Code;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.BarButtonItem btn_Print;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private WindowsApplication1.LookAndFeelSettingsHelper lookAndFeelSettingsHelper1;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown galleryDropDown1;
        private DevExpress.XtraBars.BarButtonItem btn_AddUsers;
        private DevExpress.XtraBars.BarButtonItem btn_ChangeUser;
    }
}

