﻿using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inquiry_Manger
{
    public   class LoginUserControl : XtraUserControl
    {
        public LoginUserControl()
        {
             LayoutControl lc = new LayoutControl();
             lc.Dock = DockStyle.Fill;
             Label lbl = new Label();
             lbl.Text = "Please Type Your UserName and Password ";
             lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
             tePassword.Properties.UseSystemPasswordChar = true;
          
             lc.AddItem("UserName", teLogin).TextVisible = true ;
             lc.AddItem("Password", tePassword).TextVisible = true;
             this.Controls.Add(lc);
             this.Size = new System.Drawing.Size(300, 75);
            
          // this.Dock = DockStyle.Fill;
        }
        public TextEdit teLogin = new TextEdit();
        public TextEdit tePassword = new TextEdit();
        public   static void ShowLogin()
        {
            LoginUserControl myControl = new LoginUserControl();
            UserLookAndFeel lkf = new UserLookAndFeel(myControl);
            lkf.SkinName = "The Bezier";
            ReEnter:
            if (DevExpress.XtraEditors.XtraDialog.Show(lkf, myControl, "Sign in", MessageBoxButtons.OKCancel,MessageBoxDefaultButton.Button1 ,0) == DialogResult.OK)
            {

                //string EncryptedUserName = Cryptography.Encrypt ( myControl.teLogin.Text);
                //string EncryptedUserPass = Cryptography.Encrypt(myControl.tePassword.Text);
                DataRow[] rows = Data.MainDataSet.Tables[6].Select(" Name = '" + myControl.teLogin.Text + "' and Code = '" + myControl.tePassword.Text + "'");
                if (rows.Count<DataRow>() == 1)
                {
                    Main.CurrentUser = rows[0][0].ToString();
                    Main.Isadmin = Convert.ToBoolean(rows[0][3].ToString());
                    Application.Run(new frm_Main());

                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(text: "wrong user or password " + Environment.NewLine + "Please note that the username and password are case sinsetive ", caption: "Error", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Error, defaultButton: 0);
                    goto ReEnter;
                }
 

            }

        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // LoginUserControl
            // 
            this.Name = "LoginUserControl";
            this.Size = new System.Drawing.Size(304, 126);
            this.ResumeLayout(false);

        }
    }
}

