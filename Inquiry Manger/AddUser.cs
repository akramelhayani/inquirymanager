﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors;
using DevExpress.LookAndFeel;

namespace Inquiry_Manger
{
    public partial class AddUser : UserControl
    {
        public AddUser()
        {
            LayoutControl lc = new LayoutControl();
            lc.Dock = DockStyle.Fill;
            tePassword.Properties.UseSystemPasswordChar = true;
            chk_EditAddUsers.Text = "Can Add / Edit Other users";
            lc.AddItem("UserName", teLogin).TextVisible = true;
            lc.AddItem("Password", tePassword).TextVisible = true;
            lc.AddItem(" ", chk_EditAddUsers).TextVisible = true;
            this.Controls.Add(lc);
            this.Size = new System.Drawing.Size(300, 100);
        }
        public TextEdit teLogin = new TextEdit();
        public TextEdit tePassword = new TextEdit();
        public CheckEdit chk_EditAddUsers = new CheckEdit();
        public static void ShowLogin()
        {
            AddUser myControl = new AddUser ();
            UserLookAndFeel lkf = new UserLookAndFeel(myControl);
            lkf.SkinName = "The Bezier";
            ReEnter:
            switch (DevExpress.XtraEditors.XtraDialog.Show(lkf, myControl, "Add New User ", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button1, 0))
            {
                case DialogResult.OK:
                    if (myControl.teLogin.Text.Trim() != "" && myControl.tePassword.Text.Trim() != "")
                    {
                        Data.MainDataSet.Tables[6].Rows.Add();
                        int last = Data.MainDataSet.Tables[6].Rows.Count - 1;
                        Data.MainDataSet.Tables[6].Rows[last][0] = myControl.teLogin.Text;
                        Data.MainDataSet.Tables[6].Rows[last][1] = myControl.tePassword.Text;
                        Data.MainDataSet.Tables[6].Rows[last][2] = Main.CurrentUser;
                        Data.MainDataSet.Tables[6].Rows[last][3] = myControl.chk_EditAddUsers.Checked;
                        Data.SaveMainDataFile();
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(text: "Username and Password fields can't be empty ", caption: "Error", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Error, defaultButton: 0);
                        goto ReEnter;
                    }
                    ; break;

            }



            //DataRow[] rows = Data.MainDataSet.Tables[6].Select(" Name = '" + myControl.teLogin.Text + "' and Code = '" + myControl.tePassword.Text + "'");
            //Debug.Print(rows.Count<DataRow>().ToString());
            //Application.Run(new frm_Main());


        }

        private void AddUser_Load(object sender, EventArgs e)
        {

        }
    }
}
